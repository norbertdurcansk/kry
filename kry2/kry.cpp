/**
 * Norbert Durcansky
 * 2. project KRY
 * FIT VUTBR
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <gmp.h>
#include <time.h>

#define GENERATE 10
#define ENCRYPT 11
#define DECRYPT 12
#define CRACK 13
#define HEX_BASE 16
#define HEX_OFFSET 2
#define SEED_SIZE 1024
#define SOLOVAY_ITERATIONS 50
#define SMALL_NUMBER_CHECK_SIZE 1000000

using namespace std;

gmp_randstate_t randomState;

typedef struct RSA {
    mpz_t m;
    mpz_t n;
    mpz_t e;
    mpz_t c;
    mpz_t d;
    mpz_t p;
    mpz_t q;
    mpz_t phi;
} RSA;

/**
 * Function parses command parameters
 * @return type of action
 */
int argvParser(int argc, char **argv) {
    if (argc == 3 && strcmp(argv[1], "-g") == 0) {
        return GENERATE;
    } else if (argc == 5 && strcmp(argv[1], "-e") == 0) {
        return ENCRYPT;
    } else if (argc == 5 && strcmp(argv[1], "-d") == 0) {
        return DECRYPT;
    } else if (argc == 3 && strcmp(argv[1], "-b") == 0) {
        return CRACK;
    } else {
        cerr << "./kry [-g B | -e E N M |-d D N C | -b N]\n"
                "B ... požadovaná velikost veřejného modulu v bitech (např. SEED_SIZE)\n"
                "P ... prvočíslo (při generování náhodné)\n"
                "Q ... jiné prvočíslo (při generování náhodné)\n"
                "N ... veřejný modulus\n"
                "E ... veřejný exponent (většinou 3)\n"
                "D ... soukromý exponent\n"
                "M ... otevřená zpráva (číslo, nikoli text)\n"
                "C ... zašifrovaná zpráva (číslo, nikoli text)\n";
        return 1;
    }
}

/**
 * Function initialize random seed from the current time
 */
void initRandomSeed() {
    mpz_t seed;
    mpz_init(seed);
    gmp_randinit_mt(randomState);
    unsigned char seedArray[SEED_SIZE];
    srand(time(NULL));
    for (int i = 0; i < SEED_SIZE; i++) {
        seedArray[i] = rand() % 0xff;
    }
    mpz_import(seed, SEED_SIZE, 1, sizeof(seedArray[0]), 0, 0, seedArray);
    gmp_randseed(randomState, seed);
}

/**
 * Function returns random value
 */
void getRandomValue(mpz_t *value, int size, bool isExponent) {

    unsigned char *randomValueArray = (unsigned char *) malloc(sizeof(unsigned char) * size);
    for (int i = 0; i < size; i++) {
        randomValueArray[i] = rand() % 0xff;
    }
    /** helps generate exact size of bits */
    if (!isExponent) {
        randomValueArray[0] &= 0xDF;
        randomValueArray[0] |= 0xC0;
        randomValueArray[size - 1] |= 0x01;
    }

    mpz_import((*value), size, 1, sizeof(randomValueArray[0]), 0, 0, randomValueArray);
    free(randomValueArray);
}

/** Initialize RSA structure */
void initRSA(RSA *rsa) {
    mpz_init(rsa->c);
    mpz_init(rsa->d);
    mpz_init(rsa->m);
    mpz_init(rsa->n);
    mpz_init(rsa->p);
    mpz_init(rsa->q);
    mpz_init(rsa->e);
    mpz_init(rsa->phi);
}

/**
 * Function encrypts/decrypts input message/cipher
 */
void encryptDecrypt(char *exponent, char *modulus, char *message) {
    RSA rsa;
    initRSA(&rsa);
    mpz_init_set_str(rsa.m, message + HEX_OFFSET, HEX_BASE);
    mpz_init_set_str(rsa.e, exponent + HEX_OFFSET, HEX_BASE);
    mpz_init_set_str(rsa.n, modulus + HEX_OFFSET, HEX_BASE);
    mpz_powm(rsa.c, rsa.m, rsa.e, rsa.n);
    gmp_printf("0x%Zx\n", rsa.c);
}

/**
 * Function implements jacobi function for primality test
 */
void getJacobi(mpz_t a, mpz_t n, mpz_t *jacobi) {
    // copies
    mpz_t copyA;
    mpz_init(copyA);
    mpz_set(copyA, a);
    mpz_t copyN;
    mpz_init(copyN);
    mpz_set(copyN, n);
    // temporary variables
    mpz_t tmpN;
    mpz_init(tmpN);
    mpz_t tmpA;
    mpz_init(tmpA);

    // a<0 => 0
    if (!(mpz_cmp_si(copyA, 0) > 0)) {
        mpz_set_ui(*jacobi, 0);
        mpz_clear(copyA);
        mpz_clear(copyN);
        mpz_clear(tmpN);
        mpz_clear(tmpA);
        return;
    }
    int ans = 1;
    if (mpz_cmp_si(copyA, 0) < 0) {
        //a=-a
        mpz_mul_si(copyA, copyA, -1);
        //n%4
        mpz_mod_ui(tmpN, copyN, 4);
        if (mpz_cmp_si(tmpN, 3) == 0) {
            ans = -ans;
        }
    }
    if (mpz_cmp_si(copyA, 1) == 0) {
        mpz_set_si(*jacobi, ans);
        mpz_clear(copyA);
        mpz_clear(copyN);
        mpz_clear(tmpN);
        mpz_clear(tmpA);
        return;
    }
    while (mpz_cmp_ui(copyA, 0) != 0) {
        if (mpz_cmp_si(copyA, 0) < 0) {
            //a=-a
            mpz_mul_si(copyA, copyA, -1);
            //n%4
            mpz_mod_ui(tmpN, copyN, 4);
            if (mpz_cmp_si(tmpN, 3) == 0) {
                ans = -ans;
            }
        }
        mpz_mod_ui(tmpA, copyA, 2);
        while (mpz_cmp_si(tmpA, 0) == 0) {
            // a = a/2
            mpz_div_ui(copyA, copyA, 2);
            mpz_mod_ui(tmpN, copyN, 8);
            if (mpz_cmp_si(tmpN, 3) == 0 || mpz_cmp_si(tmpN, 5) == 0) {
                ans = -ans;
            }
            mpz_mod_ui(tmpA, copyA, 2);
        }
        mpz_set(tmpN, copyN);
        mpz_set(copyN, copyA);
        mpz_set(copyA, tmpN);
        mpz_mod_ui(tmpA, copyA, 4);
        mpz_mod_ui(tmpN, copyN, 4);
        if (mpz_cmp_si(tmpN, 3) == 0 && mpz_cmp_si(tmpA, 3) == 0) {
            ans = -ans;
        }
        mpz_mod(copyA, copyA, copyN);
        mpz_div_ui(tmpN, copyN, 2);
        if (mpz_cmp(copyA, tmpN) > 0) {
            mpz_sub(copyA, copyA, copyN);
        }
    }
    if (mpz_cmp_si(copyN, 1) == 0) {
        mpz_set_si(*jacobi, ans);
        mpz_clear(copyA);
        mpz_clear(copyN);
        mpz_clear(tmpN);
        mpz_clear(tmpA);
        return;
    }
}

/**
 * Function implements Solovay-Strassen Primality Test
 */
bool SolovayPrimalityTest(mpz_t prime) {

    mpz_t a;
    mpz_t mod;
    mpz_t tmpPrime;
    mpz_t jacobi;
    mpz_init(tmpPrime);
    mpz_init(jacobi);
    mpz_init(a);
    mpz_init(mod);

    if (mpz_cmp_ui(prime, 2) < 0) {
        mpz_clear(a);
        mpz_clear(mod);
        mpz_clear(tmpPrime);
        mpz_clear(jacobi);
        return false;
    }
    if (mpz_cmp_ui(prime, 2) != 0 && mpz_even_p(prime) > 0) {
        mpz_clear(a);
        mpz_clear(mod);
        mpz_clear(tmpPrime);
        mpz_clear(jacobi);
        return false;
    }

    for (int i = 0; i < SOLOVAY_ITERATIONS; i++) {
        mpz_sub_ui(tmpPrime, prime, 1);
        mpz_set_ui(a, rand());
        mpz_mod(a, a, tmpPrime);
        mpz_add_ui(a, a, 1);

        getJacobi(a, prime, &jacobi);

        mpz_add(jacobi, jacobi, prime);
        mpz_mod(jacobi, jacobi, prime);

        mpz_sub_ui(tmpPrime, prime, 1);
        mpz_div_ui(tmpPrime, tmpPrime, 2);
        mpz_powm(mod, a, tmpPrime, prime);

        if (!(mpz_cmp_si(jacobi, 0) > 0) || mpz_cmp(mod, jacobi) != 0) {
            mpz_clear(a);
            mpz_clear(mod);
            mpz_clear(tmpPrime);
            mpz_clear(jacobi);
            return false;
        }
    }
    mpz_clear(a);
    mpz_clear(mod);
    mpz_clear(tmpPrime);
    mpz_clear(jacobi);
    return true;
}

/**
 * Function returns random prime number for specific size
 */
void getPrimeNumber(mpz_t *number, int primeSize) {

    getRandomValue(number, primeSize, false);
    bool isPrime = SolovayPrimalityTest(*number);

    while (!isPrime) {
        // add +2 and try another one
        mpz_add_ui(*number, *number, 2);
        isPrime = SolovayPrimalityTest(*number);
    }
}

/**
 * Function implements phi(n)=(p-1)*(q-1)
 */
void getPhi(mpz_t p, mpz_t q, mpz_t *phi) {
    mpz_t tmpP;
    mpz_init(tmpP);
    mpz_t tmpQ;
    mpz_init(tmpQ);

    mpz_sub_ui(tmpP, p, 1);
    mpz_sub_ui(tmpQ, q, 1);
    mpz_mul(*phi, tmpP, tmpQ);

    mpz_clear(tmpP);
    mpz_clear(tmpQ);
}

/**
 * Function returns multiplicative inverse to prime number
 * Using extended Euclid algorithm
 */
void multiplicativeInverse(mpz_t a, mpz_t m, mpz_t *inverse) {
    mpz_t m0;
    mpz_init(m0);
    mpz_set(m0, m);
    mpz_t x;
    mpz_init(x);
    mpz_set_ui(x, 1);
    mpz_t y;
    mpz_init(y);
    mpz_set_ui(y, 0);
    mpz_t copyA;
    mpz_init(copyA);
    mpz_set(copyA, a);
    mpz_t copyM;
    mpz_init(copyM);
    mpz_set(copyM, m);
    mpz_t q;
    mpz_init(q);
    mpz_t t;
    mpz_init(t);

    if (mpz_cmp_si(copyM, 1) == 0) {
        mpz_set_si(*inverse, 0);
        mpz_clear(m0);
        mpz_clear(x);
        mpz_clear(y);
        mpz_clear(copyA);
        mpz_clear(copyM);
        mpz_clear(q);
        mpz_clear(t);
        return;
    }
    while (mpz_cmp_si(copyA, 1) > 0) {

        mpz_div(q, copyA, copyM);
        mpz_set(t, copyM);
        mpz_mod(copyM, copyA, copyM);
        mpz_set(copyA, t);
        mpz_set(t, y);
        mpz_mul(y, q, y);
        mpz_sub(y, x, y);
        mpz_set(x, t);
    }
    if (mpz_cmp_si(x, 0) < 0) {
        mpz_add(x, x, m0);
    }
    mpz_set(*inverse, x);
    mpz_clear(m0);
    mpz_clear(x);
    mpz_clear(y);
    mpz_clear(copyA);
    mpz_clear(copyM);
    mpz_clear(q);
    mpz_clear(t);
    return;
}

/**
 * Function returns gcd of two large numbers
 */
void getGcd(mpz_t *result, mpz_t n1, mpz_t n2) {
    mpz_t copyN1;
    mpz_t copyN2;
    mpz_init(copyN1);
    mpz_init(copyN2);
    mpz_t r;
    mpz_init(r);
    mpz_set(copyN1, n1);
    mpz_set(copyN2, n2);

    while (mpz_cmp_si(copyN2, 0) != 0) {
        mpz_mod(r, copyN1, copyN2);
        mpz_set(copyN1, copyN2);
        mpz_set(copyN2, r);
    }
    mpz_set(*result, copyN1);
    mpz_clear(copyN1);
    mpz_clear(copyN2);
    mpz_clear(r);
    return;
}

/**
 * Functions returns random Exponent where gcd(phi(n),e)==1
 */
void getRandomExponent(mpz_t *exponent, mpz_t phi) {
    mpz_t gcd;
    mpz_init(gcd);
    while (mpz_cmp_ui(gcd, 1) != 0 || mpz_cmp(*exponent, phi) < 0) {
        getRandomValue(exponent, mpz_sizeinbase(phi, 2) / 8, true);
        getGcd(&gcd, phi, *exponent);
    }
    mpz_clear(gcd);
}

/**
 * Function generates P Q N E D
 */
void generate(char *B) {
    if (atoi(B) < 16) {
        cerr << "Minimum size is 16 bits\n";
        exit(1);
    }
    int publicModuloSize = atoi(B) / 8;
    int primeSize = publicModuloSize / 2;
    RSA rsa;
    initRSA(&rsa);
    initRandomSeed();
    getPrimeNumber(&(rsa.p), primeSize);
    getPrimeNumber(&(rsa.q), primeSize);
    // public modulus
    mpz_mul(rsa.n, rsa.p, rsa.q);
    // phi
    getPhi(rsa.p, rsa.q, &(rsa.phi));
    getRandomExponent(&(rsa.e), rsa.phi);
    multiplicativeInverse(rsa.e, rsa.phi, &(rsa.d));
    gmp_printf("0x%Zx 0x%Zx 0x%Zx 0x%Zx 0x%Zx\n", rsa.p, rsa.q, rsa.n, rsa.e, rsa.d);
}

/**
 * Function implements y = ((y^2)%n+z)%n
 */
void brentFunctionX(mpz_t varY, mpz_t varZ, mpz_t publicModulus) {
    mpz_mul(varY, varY, varY);
    mpz_mod(varY, varY, publicModulus);
    mpz_add(varY, varY, varZ);
    mpz_mod(varY, varY, publicModulus);
}

/**
 * Brent Function
 * Function is optimized Pollard's rho algorithm
 */
bool brentFactorization(mpz_t publicModulus, mpz_t *divider) {

    if (mpz_cmp_ui(publicModulus, 1) == 0) {
        // prime divider was not found
        return false;
    }
    if (mpz_even_p(publicModulus) > 0) {
        mpz_set_ui(*divider, 2);
        return true;
    }

    mpz_t varX;
    mpz_t varY;
    mpz_t varZ;
    mpz_t varV;
    mpz_t varA;
    mpz_t varB;
    mpz_t varC;
    mpz_t index;
    mpz_t step;
    mpz_init(varX);
    mpz_init(varY);
    mpz_init(index);
    mpz_init(step);
    mpz_init(varZ);
    mpz_init(varV);
    mpz_init(varA);
    mpz_init(varB);
    mpz_init(varC);
    mpz_set_ui(varA, 1);
    mpz_set_ui(varC, 1);
    mpz_set_ui(varB, 1);
    mpz_t tmp;
    mpz_init(tmp);
    mpz_t tmpB;
    mpz_init(tmpB);
    mpz_t tmpY;
    mpz_init(tmpY);
    unsigned long int size = mpz_sizeinbase(publicModulus, 2) / 8;

    getRandomValue(&varY, size, false);
    getRandomValue(&varZ, size, false);
    getRandomValue(&varV, size, false);

    while (mpz_cmp_ui(varC, 1) == 0) {
        mpz_set(varX, varY);
        while (mpz_cmp(index, varB) < 0) {
            brentFunctionX(varY, varZ, publicModulus);
            mpz_add_ui(index, index, 1);
        }
        mpz_set_ui(step, 0);
        while (mpz_cmp(step, varB) < 0 && mpz_cmp_ui(varC, 1) == 0) {
            mpz_set(tmpY, varY);

            mpz_sub(tmpB, varB, step);
            if (mpz_cmp(varV, tmpB) <= 0) {
                mpz_set(tmpB, varV);
            }
            mpz_set_ui(index, 0);
            while (mpz_cmp(index, tmpB) < 0) {
                brentFunctionX(varY, varZ, publicModulus);
                mpz_sub(tmp, varX, varY);
                mpz_abs(tmp, tmp);
                mpz_mul(varA, varA, tmp);
                mpz_mod(varA, varA, publicModulus);
                mpz_add_ui(index, index, 1);
            }
            getGcd(&varC, varA, publicModulus);
            mpz_add(step, step, varV);
        }
        mpz_mul_ui(varB, varB, 2);
    }
    if (mpz_cmp(varC, publicModulus) == 0) {
        while (true) {
            brentFunctionX(tmpY, varZ, publicModulus);
            mpz_sub(tmp, varX, tmpY);
            mpz_abs(tmp, tmp);
            getGcd(&varC, tmp, publicModulus);
            if (mpz_cmp_ui(varC, 1) > 0) {
                break;
            }
        }
    }
    mpz_set(*divider, varC);
    return true;
}

/**
 * Function crack RSA public modulus
 */
void crack(char *publicModulus) {
    mpz_t n;
    mpz_init(n);
    mpz_init_set_str(n, publicModulus + HEX_OFFSET, HEX_BASE);
    mpz_t divider;
    mpz_init(divider);
    // initial divider is 2
    mpz_set_ui(divider, 2);
    mpz_t remainder;
    mpz_init(remainder);

    //method to check small primes
    mpz_mod(remainder, n, divider);

    if (mpz_cmp_ui(remainder, 0) != 0) {
        mpz_set_ui(divider, 3);
        for (int i = 0; i <= SMALL_NUMBER_CHECK_SIZE; i++) {
            mpz_mod(remainder, n, divider);
            if (mpz_cmp_ui(remainder, 0) == 0) {
                break;
            }
            mpz_add_ui(divider, divider, 2);
        }
    }
    if (mpz_cmp_ui(remainder, 0) == 0) {
        gmp_printf("0x%Zx\n", divider);
        return;
    }
    // if not found try Brent algorithm
    if (brentFactorization(n, &divider)) {
        gmp_printf("0x%Zx\n", divider);
    } else {
        cerr << "Could not find prime number\n";
    }
}

/**
 * Main function
 */
int main(int argc, char *argv[]) {
    switch (argvParser(argc, argv)) {
        case GENERATE:
            generate(argv[2]);
            break;
        case ENCRYPT:
            encryptDecrypt(argv[2], argv[3], argv[4]);
            break;
        case DECRYPT:
            encryptDecrypt(argv[2], argv[3], argv[4]);
            break;
        case CRACK:
            crack(argv[2]);
            break;
        default:
            return 1;
    }
    return 0;
}