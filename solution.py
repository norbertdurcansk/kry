# Norbert Durcansky
# xdurca01
# 1.4.2018

import sys
import os.path

DIRECTORY = "in/"
BLOCK_SIZE = 32
BLOCK_SIZE_BITS = 32 * 8


class StreamCracker:
    def __init__(self):
        # read file to get 32 bytes of KeyStream
        self.decryptedFile = self.__readFile(DIRECTORY + "bis.txt")
        self.encryptedFile = self.__readFile(DIRECTORY + "bis.txt.enc")
        # array from decrypted super_cipher.py
        self.SUB = [0, 1, 1, 0, 1, 0, 1, 0]

    def __readFile(self, fileName):
        if os.path.isfile(fileName):
            return open(fileName, 'rb').read(BLOCK_SIZE)
        else:
            sys.stderr.write("Could not open the file " + fileName)
            sys.exit(1)

    def __getKeystream(self):
        return int.from_bytes(self.decryptedFile, 'little') ^ int.from_bytes(self.encryptedFile, 'little')

    # Function reverses step function, and gets initial Key
    def getKey(self):
        currentKey = self.__getKeystream()
        for i in range(BLOCK_SIZE_BITS // 2):
            currentKey = self.__reverseStep(currentKey)
        currentKey = currentKey.to_bytes(BLOCK_SIZE, 'little').decode()
        print(currentKey[:currentKey.rindex("}") + 1], end="")

    # Function retrieves valid indexes in SUB array
    def __getValidIndexes(self, lastBitY):
        validIndexes = []
        for index in range(len(self.SUB)):
            if self.SUB[index] == lastBitY:
                validIndexes.append(index)

        return validIndexes

    # Function returns valid values where 2lsb bits of index eq 2msb bits of X
    def __getValidValues(self, lastBitY, upperBitsX, validValue, bit):
        nextValidValues = []
        for index in range(len(self.SUB)):
            lowerBitsIndex = index & 0b11
            if self.SUB[index] == lastBitY and lowerBitsIndex == upperBitsX:
                validValue |= (index << bit)
                nextValidValues.append(validValue)
        return nextValidValues

    def __getReversedX(self, validValues):
        for value in validValues:
            if (value >> BLOCK_SIZE_BITS) == (value & 0b11):
                # mask to get only N bits
                mask = ~(1 << BLOCK_SIZE_BITS)
                invertedOperation = value >> 1
                reversedX = invertedOperation & mask
                return reversedX
        return None

    def __reverseStep(self, value):
        # inital valid values are indexes
        validValues = self.__getValidIndexes(value & 0b1)

        for bit in range(1, BLOCK_SIZE_BITS):
            nextValidValues = []
            lastBitY = (value >> bit) & 0b1
            for validValue in validValues:
                # 2msb bits
                upperBitsX = (validValue >> bit) & 0b011
                # extend valid values
                nextValidValues.extend(self.__getValidValues(lastBitY, upperBitsX, validValue, bit))
            validValues = nextValidValues
        return self.__getReversedX(validValues)


# Main function
if __name__ == '__main__':
    streamCracker = StreamCracker()
    streamCracker.getKey()
