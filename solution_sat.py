# Norbert Durcansky
# xdurca01
# 1.4.2018

from z3 import *

DIRECTORY = "in/"
BLOCK_SIZE = 32
BLOCK_SIZE_BITS = 32 * 8
SHIFT_OFFSET = 2


class SatSolver:
    def __init__(self):
        self.initialKeyStream = self.__getKeyStream()
        # Shifts in step function requires +2 bits to work
        self.keyStream = BitVec('keyStream', BLOCK_SIZE_BITS + SHIFT_OFFSET)

    def __getKeyStream(self):
        # get 32 bytes of keyStream
        plainText = open(DIRECTORY + "bis.txt", "rb").read(32)
        cipherText = open(DIRECTORY + "bis.txt.enc", "rb").read(32)
        return int.from_bytes(plainText, 'little') ^ int.from_bytes(cipherText, 'little')

    def getKey(self):
        initialKey = self.initialKeyStream
        currentKey = self.step(self.keyStream)
        for i in range(BLOCK_SIZE_BITS // 2):
            solver = Solver()
            # conditions
            # Top 2msb eq 0 -> just 256bits
            solver.add((satSolver.keyStream & (0b11 << 256)) == 0)
            solver.add(initialKey == currentKey)
            # get result
            solver.check()
            initialKey = (solver.model()[satSolver.keyStream]).as_long()
        initialKey = initialKey.to_bytes(BLOCK_SIZE, 'little').decode()
        print(initialKey[:initialKey.rindex("}") + 1], end="")

    def step(self, x):
        # Table of SUB rewritten to bit0^(bit1|bit2)
        # 111,101,011,000 -> 0
        # 001,010,100,110 -> 1
        x = (x & 1) << BLOCK_SIZE_BITS + 1 | x << 1 | x >> BLOCK_SIZE_BITS - 1
        y = 0
        for i in range(BLOCK_SIZE_BITS):
            index = (x >> i)
            indexBit0 = (index & 0b001)
            indexBit12 = (index & 0b010) >> 1 | (index & 0b100) >> 2
            subValue = indexBit12 ^ indexBit0
            y |= subValue << i
        return y


# Main function
if __name__ == '__main__':
    satSolver = SatSolver()
    satSolver.getKey()
